function flip(element) {
    element.classList.toggle('pf-skills-card-is-flipped');
};

function toggleMenu() {
    const mobMenu = document.querySelector(".pf-header-menu-mobile");
    mobMenu.classList.toggle('pf-hide');
};

function backToTop() {
    window.scrollTo(0, 0);
}